package cmd

func mergeMaps(map1, map2 map[string]any) map[string]any {
	mergedMap := make(map[string]interface{})
	for key, val := range map1 {
		mergedMap[key] = val
	}
	for key, val := range map2 {
		if _, ok := mergedMap[key]; ok {
			if subMap1, ok := val.(map[string]any); ok {
				if subMap2, ok := mergedMap[key].(map[string]any); ok {
					mergedMap[key] = mergeMaps(subMap2, subMap1)
				} else {
					mergedMap[key] = val
				}
			} else {
				mergedMap[key] = val
			}
		} else {
			mergedMap[key] = val
		}
	}
	return mergedMap
}

func mergeStringMaps(map1 map[string]string, map2 map[string]string) map[string]string {
	mergedMap := make(map[string]string)
	for k, v := range map1 {
		mergedMap[k] = v
	}
	for k, v := range map2 {
		mergedMap[k] = v
	}
	return mergedMap
}
