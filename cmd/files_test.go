package cmd

import (
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
	"os"
	"testing"
)

func TestSealedFile_containsKeyStrForValueFile(t *testing.T) {
	f, _ := os.CreateTemp("", "_sealed.yaml")
	ff, _ := os.CreateTemp("", "dev.yaml")
	defer f.Close()
	defer ff.Close()

	sf := &sealedFile{
		File:     f,
		contents: make(map[string][]string),
	}
	vf := &valueFile{
		File: ff,
		name: "dev.yaml",
	}

	sf.addSecretValueKey("dev.yaml", "testSecret")

	assert.Equal(t, true, sf.containsKeyStrForValueFile("testSecret", vf))
}

func TestValueFile_findValueByKeyStr(t *testing.T) {
	vf := &valueFile{
		name: "dev.yaml",
	}

	devYaml := `
someObj:
  myNewSecret: newSecret
  otherSecret: foo
  foo:
    bar: baz
`

	err := yaml.Unmarshal([]byte(devYaml), &vf.document)
	assert.NoError(t, err)

	value, ok := vf.findValueByKeyString("someObj.foo.bar")
	assert.Equal(t, true, ok)
	assert.Equal(t, "baz", value)
}
func TestValueFile_setValueByKeyString(t *testing.T) {
	vf := &valueFile{
		name: "dev.yaml",
	}

	devYaml := `
someObj:
  myNewSecret: newSecret
  otherSecret: foo
  foo:
    bar: baz
`

	err := yaml.Unmarshal([]byte(devYaml), &vf.document)
	assert.NoError(t, err)

	err = vf.setValueByKeyString("someObj.foo.bar", "newVal")
	assert.NoError(t, err)

	value, ok := vf.findValueByKeyString("someObj.foo.bar")
	assert.Equal(t, true, ok)
	assert.Equal(t, "newVal", value)
}
